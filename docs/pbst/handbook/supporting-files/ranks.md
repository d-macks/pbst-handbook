# 🎖 Ranks

## Standard Ranks
### Cadet
 - This is the default rank you get on joining.
 - Requirements: 
   * None, this is the starting rank. You'll get ranked this when you join. 
 - You wont get any good weapons, the only tools you get are: 
   * Cadet Baton (***Requirement***: Be in [PBST](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team))
     * This weapon is in the low-damage category, you'll do almost no damage with this. 
   * Medkit (***Requirement***: Be in the [PB Main Group](https://www.roblox.com/groups/159511/Pinewood-Builders) and [PBST](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team))


## Tiers
The Tier ranks are the medium ranks of PBST. Tiers are expected to be a role model for Cadets. Any violations of the rules may result in a larger punishment. Especially Tier 3’s and Elites are expected to be the perfect representation of PBST at its best. **You have been warned.**

The loadouts for the different Tier ranks can be found in the Security room at Pinewood facilities. Cadets are not allowed in the loadout rooms of Tiers.

Tiers have access to the ``!call PBST`` command, which they can use for an imminent melt- or freezedown or a raider. This is to be used wisely.

As a Tier, you may be selected to assist at a training or an eval, where you will be given temporary admin powers which are strictly for that specific training. Abuse of these powers will result in severe punishment.

### Becoming a Tier
Once you reach 100 points, you must participate in a Tier evaluation if you want to get the Tier 1 rank. It is not recommended to get more points before your evaluation, if you take the eval while having more than 150 points you’ll be set back to 150.

As with normal trainings, the scheduled Tier evaluations can be found on the schedule at [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). They will be hosted regularly to accommodate all timezones. **There are no points to be earned in a Tier eval**.

There is a specialized server for Tier evaluations, use the command !pbstevalserver to get there. This only works if you have 100+ points.

A Tier evaluation consists of 3 parts: a quiz, a combat eval, and a test on patrolling skills. The training rules will be heavily enforced in this eval, not following them will result in an immediate fail. If you pass some parts part but fail others, you may try again at another eval and skip any part you passed before.

During the quiz, you will receive 8 questions about various topics including this handbook and training rules. The questions will vary in difficulty, some are easy but others require more thinking. You need to score at least 5/8 to pass. Answering must be done privately through whisper chat or a Private Message system sent by the host.

During the combat eval, you will face 3 combat-related challenges: complete a Level 1 SF Bot Arena alone, survive Bomb Barrage with increasing levels, and score as many hits as possible in the Firing Range within 60 seconds. Each completed challenge will give points, as presented in this chart:

![img](pbst-tier-eval.png)

During the patrol test, you will receive Tier 1 loadouts and be tested on skills like your abilities in combat, teamwork, and following the handbook correctly. The Tier eval assistants will present you with various situations where you have to react to appropriately.

:::danger Be warned that punishments are worse when you're an higher rank
Since you've been promoted to Tier 1 (If you are). You'll be punished worse if you violate the handbook, depending on the violation. You can either get a point deduction, demotion, blacklist or a trello/global ban.

[**More information about punishments can be found here**](supporting-files/punishments.md)

If you've been banned on any of these platforms, you'll be able to contact PIA on the [Pinewood Builders Appeals Center (PBAC)](https://discord.gg/mWnQm25) discord.
:::

### Tier 1
 - This is the first rank that you'll be able to earn within PBST with points.
 - Requirements: 
   * 100+ points
   * Passed Tier Evaluations:
     * Quiz (5 out of 8 questions correct)
     * Patrol (Pass a patrol evaluation)
     * Combat ([See this image](https://i.imgur.com/Ft6k5Ov.png))
 - You'll be given access to a few better weapons:
   * A higher damage baton
   * A taser (A weapon that will stun the person hit for a few seconds)
   * PBST Riot shield (Defend against guns/tasers)
   * A pistol (Simple pistol)
 - You'll be allowed to assist in tier evals!


### Tier 2
 - This is the second rank that you'll be able to earn within PBST with points.
 - Requirements: 
   * 250+ points
 - You'll be given access to ***an extra weapon, all weapons that are bold are taken from the T1 loadout***:
   * ***A higher damage baton***
   * ***A taser (A weapon that will stun the person hit for a few seconds)***
   * ***PBST Riot shield (Defend against guns/tasers)***
   * ***A pistol (Simple pistol)***
   * PBST Rifle

### Tier 3
 - This is the third and last rank that you'll be able to earn within PBST with points.
 - Requirements: 
   * 500+ points
 - You'll be given access to ***an extra weapon, all weapons that are bold are taken from the T2 loadout***:
   * ***A higher damage baton***
   * ***A taser (A weapon that will stun the person hit for a few seconds)***
   * ***PBST Riot shield (Defend against guns/tasers)***
   * ***A pistol (Simple pistol)***
   * ***PBST Rifle***
   * Submachine Gun

### Assisting during a Tier evaluation
***NOTE: Assisting Tier evals will be done using a Discord server, which is therefore required.***

Tier 1+ can volunteer to assist a Tier eval through the comms server. Their most important task is to play out all the scenarios in the patrol part of the eval. They also help out the host with the usual stuff.

Asssisting a Tier eval will get you points depending on how long you assisted. ***Only the assistants can earn points in the Tier eval.***
30 Minutes - 3 points
1 Hour - 5 points
1 Hour and 30 minutes - 7 points
2 Hours - 10 points
2 Hours and 30 minutes - 13 points
3 Hours - 15 points

## Elite Tiers
Elite Tiers are handpicked from current Tier 3’s. This rank is still in development, and more information will be added soon. Here’s what’s already confirmed:

SD’s and RRL’s can suggest Tier 3’s for Elite Tier 1. Trainers will vote on their promotion.
SD’s and RRL’s can suggest Elite Tiers 1 for Elite Tier 2. Trainers will vote on their promotion.
The Elite Tier ranks will each have a unique uniform that only they can wear.
Elite Tiers can issue Kill on Sight orders **during raids only, when no Raid Response Leaders or Special Defense+ are present.**

### Elite Tier 1
Elite Tier 1: Handpicked. In development, more info coming soon.

### Elite Tier 2
Elite Tier 2: Handpicked. In development, more info coming soon.

## HR Ranks
### Special Defense
Special Defense is the first HR rank, which can only be reached by being in the Discord server and having 2-step verification on **all relevant connected accounts.**

Tier 3’s who reach 800 points are eligible for an SD evaluation to become Special Defense. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you will either be promoted directly or be given the title “Passed SD Eval” until the Trainers choose to promote you to SD.

As an SD, you receive the ability to place a KoS in Pinewood facilities, you can order room restrictions, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used responsibly.

SD’s can host events like trainings and Tier evals with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. There also has to be a 1-hour gap at least between the end of one event and the start of another, so events take place every other hour at most.

To summarize, the following things are the primary duties for all SD’s:

 * Hosting (if granted permission) and raid response leading. One of these two should be done AT LEAST once a month unless there is a valid inactivity notice or valid excuse supplied by a higher-ranking official.  
 * Reward/Punishment requests for lower ranks. SDs are encouraged to do this but they aren’t exactly forced to. Though, if they are purposefully ignoring an extreme case over favoritism then they will be warned/punished.  
Additionally, SD’s have the ability to moderate the Discord server, though this is secondary to their other duties.

### Trainers
To become a Trainer, all the current Trainers have to vote on your promotion. Only SD’s are eligible for this promotion. Should a Trainer go on an extended leave, they may maintain their rank, or leave and have their rank restored upon returning.

Trainers are the leadership of the group and as a result, they have the following primary duties:

 * Participation in votes. A vote on votes should be placed most of the time, if the trainer has no opinion then use the abstain feature. Votes can be overridden by the HoS when he provides a valid reason.  
 * Handling operations, promotions, points, and being responsible for all changes done to the group.  
 * Hosting events, this must be done AT LEAST once a month unless there is a valid inactivity notice or valid excuse by higher-ranking official. Trainers do not have any of the SD’s restrictions on this.  
Additionally, Trainers can moderate the comms server as well as enforce the handbook in-game, though again these are secondary to their main duties.

### HOS
Head of Security: Chosen by Owner. Unobtainable.

### Owner
Owner: Diddleshot.
